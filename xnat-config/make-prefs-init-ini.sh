#!/bin/sh

cat > ${CONFIG_DIR}/prefs-init.ini << EOF
#
# prefs-init.ini
# XNAT http://www.xnat.org
# Copyright (c) 2016, Washington University School of Medicine
# All Rights Reserved
#
# Released under the Simplified BSD.
#
[siteConfig]
siteUrl=${SITE_URL}
siteId=${SITE_ID}
adminEmail=${ADMIN_EMAIL}
archivePath=${ARCHIVE_PATH}
buildPath=${BUILD_PATH}
cachePath=${CACHE_PATH}
ftpPath=${CACHE_PATH}
pipelinePath=${PIPELINE_PATH}
prearchivePath=${PREARCHIVE_PATH}
security.channel=any

[notifications]
hostname=mailhost
port=25
protocol=smtp
EOF