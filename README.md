XNAT Skaffold
=============

A stab at getting a local XNAT helm build for plugin development.

# How to start

Adapted from the AIS [XNAT Quick Start Guide](https://australian-imaging-service.github.io/docs/charts/deployment/xnat_quick_start_guide/) and [XNAT Chart README](https://australian-imaging-service.github.io/docs/charts/development/xnat-chart-readme/).

## Setup
Install helm, docker, and kubectl
```shell
brew install helm kubectl
```

Add the helm chart repository for bitnami—these are standard publicly available charts.
```shell
helm repo add bitnami https://charts.bitnami.com/bitnami
```
Note that I don't know if it's necessary to add the bitnami charts; they may come configured already with stock helm. But just in case.

Now update the repos.
```shell
helm repo update
```
This is how your local helm discovers updates that may have been pushed to the chart repositories. You might want to run this periodically to keep up to date.

### Docker + Kubernetes
Install your docker of choice and start it up. You could use Docker Desktop. I use [colima](https://github.com/abiosoft/colima). Just follow your heart.

Let's assume you're using `colima`. Install it with homebrew.
```shell
brew install colima
```

If you don't even have docker desktop installed you'll need to also get the `docker` client CLI from homebrew.
```shell
brew install docker
```

We'll start the `colima` instance in a moment. First a diversion into kubernetes.

In addition to docker you'll need to have a kubernetes cluster running. There are several ways to do that. If you're on Docker Desktop it comes with a built-in kubernetes server that you can enable (see [Deploy on Kubernetes](https://docs.docker.com/desktop/kubernetes/) in their docs). On colima you can start with the `--kubernetes` flag. Or with any kind of docker you can always run [minikube](https://minikube.sigs.k8s.io/docs/start/). 

Let's continue with our `colima` example. Now we will start the VM with docker and kubernetes. We will need to give it an appropriate amount of resources. I have gone back and forth on how much this really needs to be. I have a machine with 32GiB of memory and an M1 CPU with 8 performance cores and 2 efficiency cores. I opt to give a ton of these resources over to the VM, even if it doesn't really use them, and so far I haven't noticed a performance degredation in my ordinary day-to-day usage on the machine. YMMV.

Anyway, choose whatever number of CPUs and amount of memory you want to give to the machine. I chose to give it 8 CPUs and 16GiB of memory, which is probably overkill, but that's ok. 

Start the VM with
```shell
colima start --cpu <number of CPUs> --memory <amount of memory in GiB> --kubernetes
```

## Build the image

Clone this repo locally. `cd` into it.

Get an XNAT war, either by building it from source or downloading from somewhere. Explode the XNAT war into a directory `xnat-exploded`.
```shell
mkdir xnat-exploded
unzip path/to/xnat-web-*.war -d xnat-exploded/
```

Get a container service jar, similarly either by building or downloading, and put it into a `plugins` directory.
```shell
mkdir plugins
cp path/to/container-service-*.jar plugins/
```

Build the docker image. 
```shell
docker build -t xnat-cs-dev:test .
```

**Note** For purposes of this repo I have hard-coded the image as `xnat-cs-dev:test`. This will obviously have to change later. If you want to use a different image repository and tag just provide them instead, like this:
```shell
docker build -t <your repository>:<your tag> .
```
In the next step you'll need to provide those to helm.

## Create volumes for archive and logs
The chart is currently set up that it will use local directories for the archive and logs.
There is a script in this repo to create the directories and the kubernetes PVs and PVCs.
```shell
./create-volumes.sh
```

## Start XNAT

Start up XNAT with helm.
```shell
helm upgrade --install xnat . --values values.yaml -nxnat --create-namespace
```

If you set your own image repository and tag earlier, you'll need to override the values in the `values.yaml` file. You can either edit the file directly and change the values in the section 
```yaml
xnat-web:
  image:
    repository: <your repository>
    tag: <your tag>
```
Or you can set the values on the command line when you start up.
```shell
helm upgrade xnat . --install --values values.yaml --set xnat-web.image.repository=<your repository> --set xnat-web.image.tag=<your tag> -nxnat --create-namespace
```

## Monitor startup
You can watch startup progess of the kubernetes objects with
```shell
watch kubectl -nxnat get all,pv,pvc
```

And once watch the XNAT startup logs with
```shell
kubectl logs --follow -nxnat xnat-xnat-web-0 -c xnat-web
```

## Log in
In a free terminal, tell kubernetes to forward port `8080` from localhost to port `80` on the xnat container.
```shell
kubectl -nxnat port-forward service/xnat-xnat-web 8080:80
```
This command will have to keep running, so either keep a dedicated terminal session open for it or put it in the background.

Open your browser to [http://localhost:8080](http://localhost:8080). XNAT should be there waiting.

## Open a shell
If you want to open a shell in the XNAT container (to look at logs or whatever) you can run this command in a free terminal.
```shell
kubectl exec -it xnat-xnat-web-0 -nxnat -- sh
```

# Stopping / Restarting / Shutting Down

Let's say you have XNAT running using the chart but you need to change something.

If you made a change to the chart or the values or whatever, you can simply redeploy the chart and that will get updated in place. To do this, run the same command you ran earlier to deploy. For example:
```shell
helm upgrade --install xnat xnat-chart --values values.yaml -nxnat
```

If you want to stop the XNAT, just uninstall the chart.
```shell
helm uninstall -nxnat xnat
```

If you want to start later use the `helm upgrade` command.

## Remove persistent files
All the files in the cache and database will persist across restarts and uninstall/reinstalls.
The archive and logs will also persist because we manually created volumes for them.

If, however, you want to remove those files too and start fresh, delete the PVCs and PVs. This will remove all PVCs in the `xnat` namespace and all PVs in your cluster:
```shell
kubectl delete pvc --all -nxnat && kubectl delete pv --all
```

If you also want to remove everything in your archive and logs (and why wouldn't you?) remove the local directories:
```shell
rm -r archive logs
```

## Shut down VM

If you're using `colima` you can stop your VM with
```shell
colima stop
```
start it again with
```shell
colima start
```
and delete it with
```shell
colima delete
```

# Notes

For my full notes related to this project, see [NOTES](NOTES.md). These will be useful to anyone trying to make this stuff work.

# NGINX Ingress Controller

Add this to the `xnat` namespace to get an ingress controller to work with XNAT + JupyterHub.

```shell
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.6.4/deploy/static/provider/cloud/deploy.yaml
kubectl apply -f nginx-ingress.yaml -n xnat
kubectl delete -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.6.4/deploy/static/provider/cloud/deploy.yaml
kubectl delete -f nginx-ingress.yaml -n xnat
```
